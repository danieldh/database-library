<?php
    /* 
        This code was written by Dani�l den Hollander
        22-01-2019
    */
	namespace Daniel;
	
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_PREFIX', '');
    define('DB_NAME', '');

    class clsDatabase
    {
        private static $oConnection = null;

        public static function init()
        {
            self::$oConnection = new Mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
            if(self::$oConnection->connect_errno)
            {
                die('CONNECTION FAILED REASON:' . self::$oConnection->connect_error);
            }
        }

        public static function execute($sql = null, $bDebug = false)
        {
            if(!empty($sql) && !empty(self::$oConnection))
            {
                    $bResult = self::$oConnection->real_query($sql);
                    if($bResult)
                    {
                        if($bDebug)
                        {
                            print_r('Query executed succesfull');
                        }

                        return true;
                    }

                if($bDebug)
                {
                    print_r('No database connection found');
                }

                return false;
            }
            
            if($bDebug)
            {
                print_r('No query found.');
            }

            return false;
        }

        public static function selectOne($sql)
        {
            return self::select($sql, true);
        }

        public static function select($sql = null, $bLimitOne = false)
        {
            if(!empty($sql) && !empty(self::$oConnection))
            {
                $aRecords = self::$oConnection->query($sql);
            }

            if($bLimitOne)
            {
                return $aRecords[0];
            }

            return $aRecords;
        }

        public static function close()
        {
            if(!empty(self::$oConnection))
            {
                self::$oConnection->close();
                return true;
            }
            
            return false;
        }
    }
?>